# Keeweb Setup

Keeweb is an open source, user-friendly password manager that "Just Works ™".

Follow the guide below to get your own password manager up and running

## Step 1: Download and Install Keeweb

To download keeweb, go to https://keeweb.info (the download link should confront you immediately)

Once downloaded, run the installer; using the installer defaults should be fine

## Step 2: Run Keeweb and access the settings menu

When you first run keeweb, you'll be greeted with a dark screen featuring a textbox and a couple of
buttons above it.

Click the New Button with the large plus sign above it to create a new password file.

<img src="/keeweb/keeweb-setup-s1.webp" width="80%"></img>

<br />
<br />
<br />

Next, you'll be taken to the main password listing screen. There's nothing in it yet, but before we
get to adding passwords, we need to setup a few things.

At the bottom left corner of the window, you'll see an open lock icon with the word New next to it -
click on that.

<img src="/keeweb/keeweb-setup-s2.webp" width="80%"></img>

## Step 3: Create your master password

We're now looking at the settings menu for our password file. From here, we'll create our master
password and give our password file a name.

You'll want to think up a strong password to use here, since the password manager will hold the
passwords to many of your online accounts. I would recommend a password of at least 15 characters
with capital and lowercase letters, numbers, and symbols. Thinking up these passwords is annoying,
but you'll only need to remember this one password from now on!

Type whatever password you come up with into the Master password textbox (and the confirm password
textbox as well)

Optional - You can change the name of your password file using the Name textbox below (Recommended)

<img src="/keeweb/keeweb-setup-s3.webp" width="80%"></img>

## Step 4: Save to Google Drive

The last step is to save our password file to google drive, so we can access it from all of our
devices.

Click the Save to button, then click Google Drive

<img src="/keeweb/keeweb-setup-s4.webp" width="80%"></img>

<br />

Your web browser will open up a google sign-in tab.  
Select the google account you want to use and follow through the steps to give keeweb authorization.

## Finished!

Whew! That's it! Your password manager is ready to be used! Because our password file lives in
google drive, we can also access our passwords from our phones.

I recommend [Keepass2Android] for android phones. For iOS, Keepassium looks nice (but I haven't
personally used it).

Now that you got your password manager up and running, you're probably wondering how exactly to use
it! Check out the Keeweb Tutorial page to learn the basics

[Keepass2Android]: https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=en_US&gl=US
