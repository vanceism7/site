# Professional Biography/Expanded Resume

---

## Skills

**Rust**: Rocket.rs, SqlX; **C#**: Entity Framework; **Go**: Gin-gonic, Gorm; **Typescript**: React,
NestJS, NextJS, RemixJS, AlpineJS, Drizzle ORM, Zod, Shad-cn; **Haskell**: Yesod, Persistent,
Effectful; **Purescript**: Halogen; **Databases**: PostgreSQL, Sqlite; **Devops**: Docker,
Kubernetes (Microk8s and Minicube), Helm, AWS, OAuth

### Projects/Git Profiles

- [Portfolio](/portfolio)
- https://gitlab.com/vanceism7
- https://github.com/vanceism7

---

## Amis Pharmacy Corporation, Principal Engineer, 2013-2018

This was my **first job** working as a programmer professionally. A small owner of a local pharmacy
hired myself and a close friend to build out a **patient management/drug dispensing** software for
him. Since we had little real world experience, we mostly arbitrarily picked **C#/WinForms** and
**MSSQL server** as our technology to develop with.

This job gave us a lot of great experience as a crash course into professional work. We learned how
to **gather requirements** from the client/owner, and interfaced with a quite a few third party
vendors. We serialized prescriptions into an obscure EHR format to file claims with insurance
companies like **Medi-Cal** and **Aetna**; serialized/deserialized prescriptions to **xml** in order
to receieve or make requests from [Surescripts][ss], which required passing a **Surescripts audit**
before being admitted to their network; setup automations to integrate a drug information database
from [MediSpan][mds] on a weekly schedule; learned to use **SQL databases** in general, initially
starting with **MSSQL server**, but eventually transitioning to **Postgres**; and learned how to use
the **Entity Framework** ORM. I also built a small **expressjs** node server to handle communication
with surescripts.

Unfortunately, we _did not_ learn much in terms of modern tech stacks as **WinForms** was so old at
the time that even it's successor **WPF** itself had already been deprecated! We also mostly ignored
modern web technologies, the end result was we basically created a huge, local desktop monolith
software. Initially the code was a mess, we built all of our logic right into the frontend, but
eventually learned the power isolating **domain logic** from the technology stack so we could start
introducing **unit tests**. Despite how ugly the whole thing was, we **deployed** our software into
4 store fronts which ran the owner's pharmacies for a few years.

Our deployment method was also awful, I basically created a small desktop application for the
pharmacy employees which essentially made a call to **git** to pull in updated code. IIRC, we also
committed our binaries and DLLs to the git repo, so that the git pull updated the software. We had
some sort of **make-shift database migration** script to update the tables when things needed to be
changed, but we worked really hard to avoid changing the schema (you can probably imagine why)!
Every pharmacy had **postgres** installed on a computer within the LAN which acted as the server for
the rest of the clients to interface with; every software client invoked the database directly.

It was about as scrappy as you could get, but overall I'd say I learned a lot of good foundational
knowledge at this job. Stuff like the **Single Responsibility Principle**, (In fact, I read [Clean
Code][cc] at this time, which really changed the way I think about code in general), **mocking
services**, and the importance of **unit testing**. I also learned a great deal of what _not to do_
when developing software. Eventually the project was cancelled when we realized the **DEA** would
charge us 50k per year to maintain certification for prescribing controlled substances, which was a
deal breaker for a pharmacy chain which was barely making ends meet as it was.

---

## CitizenNet/CondeNast, Frontend Engineer, 2019-2021

It took me a year to find my next job, and during this time off, I became quite obsessed with
**functional programming** after dealing with the onslaught of constant **bugs** we dealt with at
the pharmacy. I ended up learning and falling in love with [Haskell][hs], one of the toughest
languages in existance out in the wild, and this eventually prepared me for a job at **CitizenNet**,
where they were using [Purescript][ps], a haskell derivative, to develop frontends for their
advertisement platform.

This was my first experience working on a **larger dev team**, which featured about 30 developers
working on various backend and frontend services. We mostly worked remote, and we used **JIRA** to
track tickets and progress. We had daily stand-ups on zoom to discuss upcoming work, and biweekly
**sprint-planning** meetings. It was my first time being exposed to **RESTful** web apis, using
**json** to communicate with the backend, and being exposed to [Kubernetes][k8s] (k8s). Every developer
used an **AWS EC2** instance to host their own dev environment (**mysql** database, backend, and
frontend) with **minikube** as the k8s controller (IMO, this was a huge waste of money...) My
understanding of k8s was quite weak at that time, but it was a nice introduction to it.

I eventually became the lead frontend developer for the **Audience Studio** team, where I
collaborated with a few other backend developers. My job basically entailed building out new **UI
routes**, and creating new controls/widgets on [Purescript-Ocelot][pso], **CitizenNet's** open
source frontend library. I also **trained** a support engineer in **Purescript**, and he eventually
joined me as part of the frontend team. Lastly, I also got to implement some cool charts using [d3].
It was great to learn **REST** and get true frontend experience under my belt, but the job got old
quickly. I also didn't care much for the mission of the company, so I eventually left to join a
company which I felt had a better purpose.

---

## Tomorrow's Talent, CTO, 2021-2024

This is actually a company my father-in-law started. I had been helping him on the side during my
time at **CitizenNet** with getting a basic [website][tt] setup so he could start interfacing with
clients. This is much less of an **engineering** role and more of a general **IT** role. This
company deals in education, bridging the gap between schools and employers. I've watched the company
grow from a **4 man team** to now **over 16 employees!** I helped setup all of the
**infrastructure** we currently use, such as our corporate email account and phone system via
**google workspace**, **google chat** for day to day communications, and [Monday.com] as our **CRM**
and **project management** tool. Besides this, my work has mainly entailed **assessing** software
systems to see what best suits our needs, and aid with onboarding our team to these systems. I've
had the chance to dive a bit into **google app scripts**, writing some simple functionality
extensions to **google forms** and **google calendar**. Probably the most productive
engineering-wise experience I've gained from this position has been working on some internal **R&D**
projects.

I recently learned [rust] while developing an internal student-tracking system. It uses the
[rocket.rs] web framework to run an **api server** and to host a simpler frontend with
[AlpineJS][ajs]. I've also gotten a much better handle of using **Kubernetes** as well. I recently
deployed a **2 node cluster** consisting of a **raspberry pi 4** and an **intel n-100** mini pc
which hosts a number of services I use to aid with development of this system, services such as:
[Hashicorp Vault][hcv], [Cert-Manager][cm], [Woodpecker CI][wpci], [Keycloak][kc], and
[OpenFGA][ofga]. I haven't gotten much use out of the CI server yet, but it's been fun to implement
**google sign-in** via **keycloak** and handling fine grained **authz** via **openfga**; I
definitely learned a lot about **authentication** and **authorization** through this process. I also
had my first experience interfacing with **GraphQL** when I wrote a small **web-hook** integration
between **Monday.com** and **Mailchimp**, which I deployed on **AWS Lambda**.

## 2024 - Current

I left Tomorrow's Talent in September of 2024 due to feeling that my growth had become stagnant.
(Actually, I technically still work there, but only in minor capacity to keep the infrastructure
running.) It didn't feel any longer like I was progressing as an engineer, and I also didn't believe
it was worth it for them to continue paying me; their general activities just didn't really
necessitate heavy usage of technology in this type of way. We had reached stable point in terms of
the infrastructure we did have, so it seemed like a good time to make a graceful exit.

Since then, my latest endeavors have been pursuing a startup idea I imagined during my time at
Tomorrow's Talent. I started developing a platform which uses [NestJS] on the backend, with a
**PostgreSQL** database, and vanilla **React** on the frontend. It uses [Drizzle-ORM] to build the
database schemas and [Zod] + [Conform] on the frontend to validate client-side data. Lastly, theres
a little companion [flask] server that I've hooked up with some [huggingface] language models to
provide text similarity comparsion services.

Aside from this, I've also started contributing to the [cppfront] project. I'd taken an interest in
programming languages and compilers (since my time in Haskell), and I've had a lot of fun working on
it so far! I've began the creation of a [LSP] compliant language server for cppfront, which you can
find [here][lscpp2]. This is my first time writing **C++** in a serious manner, and my first time
creating a language server.

---

# Conclusion

Anyways, that's a bit of a deeper dive into my past work experiences! I hope this helps to **create
a better picture** of who I am and what I'm capable of. A lot of my experience has been really
scrappy, but I'm proud that I've always able to dive into these new situations and find
"good-enough" solutions to tackle our problems.

[hs]: https://haskell.org
[ps]: https://purescript.org
[ss]: https://surescripts.com/
[mds]: https://www.wolterskluwer.com/en/solutions/medi-span
[pso]: https://github.com/citizennet/purescript-ocelot
[d3]: https://d3js.org/
[tt]: https://tomorrowstalent.org
[Monday.com]: https://monday.com
[rust]: https://rust-lang.org
[rocket.rs]: https://rocket.rs
[ajs]: https://alpinejs.dev/
[k8s]: https://kubernetes.io/
[hcv]: https://www.vaultproject.io/
[wpci]: https://woodpecker-ci.org/
[kc]: https://www.keycloak.org/
[ofga]: https://openfga.dev/
[cm]: https://cert-manager.io/
[cc]: https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882
[NestJS]: https://nestjs.com/
[Drizzle-ORM]: https://orm.drizzle.team/
[Zod]: https://zod.dev/
[Conform]: https://conform.guide/
[huggingface]: https://huggingface.co/
[flask]: https://flask.palletsprojects.com/en/stable/
[LSP]: https://microsoft.github.io/language-server-protocol/
[cppfront]: https://github.com/hsutter/cppfront
[lscpp2]: https://github.com/vanceism7/ls-cpp2
