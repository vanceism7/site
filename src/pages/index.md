# Vance Palacio

Welcome to my site!

I'm a software engineer with around 10 years of experience, currently looking for my next exciting
project to work on! I enjoy tackling engineering problems, especially in languages with strong
type-systems such as [Rust][rust], [Haskell][hs], and [Typescript][ts]. I've worked on both frontend
apps (using [purescript-halogen][psh], [flutter][flutter], [React][react]) and backend services
(with go [gin][gin], rust's [rocket][rocket], and [nestjs][NestJS]). I'm a big proponent of the
functional programming style (meaning immutable variables, higher order functions, etc), and all the
benefits software gains through them, such as:

- A huge reduction of runtime bugs
- Ease of long-term maintainability
- Code simplicity and readability

I write [guides][gds] on various topics with the main focus being on productivity. They serve as a
helpful way to keep track of things I've learned, the memory is getting pesky these days...
I hope anyone who happens upon this site finds them useful!

In addition to programming, I'm also a passionate music composer! I write video game music and film
scores. Check out my stuff! https://www.youtube.com/vanceism7

If you want to get in touch with me, feel free to send me an email at vance@vanceism7.us

[rust]: https://rust-lang.org
[tt]: https://tomorrowstalent.org
[hs]: https://www.haskell.org/
[ts]: https://www.typescriptlang.org/
[ps]: https://www.purescript.org/
[flutter]: https://flutter.dev/
[React]: https://react.dev/
[gin]: https://gin-gonic.com/
[rocket]: https://rocket.rs/
[psh]: https://github.com/purescript-halogen/purescript-halogen
[gds]: ./docs/intro
[nestjs]: https://nestjs.com/
