# Portfolio

Below is a small collection of demo's and videos on different projects I've worked on.

Be sure to check out my [extended resume] as well if you're curious to see the types of projects and
technologies I've used on previous jobs in more detail

### Git Profiles

- https://gitlab.com/vanceism7
- https://github.com/vanceism7

---

## ls-cpp2

This is a LSP compliant language server I've been working on for the [cpp2] project by Herb Sutter.

`cpp2` is an experimental new syntax for c++ which automatically incorporates best-practices
concerning software safety as well as ease-of-use improvements; it also generally functions as a
test-bed for trying out new c++ proposals. `cpp2` is akin to what `typescript` is to `javascript`:
it's fully compatible with c++, allowing the seamless use of c++ libraries without any need to write
glue code/ FFI bindings, and it compiles down to c++; it's meant to have a super easy and low-risk
adoption curve.

Learn more about cpp2 at: https://hsutter.github.io/cppfront/

And also, here's the link to the ls-cpp2 repo:  
https://github.com/vanceism7/ls-cpp2

[cpp2]: https://github.com/hsutter/cppfront

---

## Todo Task

Todo Task is a Kanban-style task management app, written in flutter. It's a local-first application
which stores all of its data in plaintext json on local storage, but optionally also on google drive
if you wish to have it in the cloud.

Demo Video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/wXl_jhWIg_M?si=GuTPHQgwKTgyUrHz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Git Link: https://gitlab.com/self-reliant/todo-task

---

## Brick Breaker Extreme!

This is a simple litle brick breaker game I wrote in C with the SDL2 library. I first learned C/C++
back in high school, but hadn't touched it at all since then; so I thought this would be a fun way
to experience the language now that I had better programming chops under my belt. It was a lot of
fun! But also, a lot of simple things can be quite difficult to do with it; I guess we shouldn't
complain though seeing as C isn't too far beyond assembly. Hope you guys like it!

<iframe width="560" height="315" src="https://www.youtube.com/embed/cJgZZ1NkSjc?si=7Fok-NHqMXgHfKyX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Git Link: https://gitlab.com/vanceism7/brick-breaker

[extended resume]: /work-history
