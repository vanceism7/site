---
tags: [blog]
date: 2022-03-04
---

# The learning ladder

This post is part 1 of a 2 part series on my thoughts about learning as it applies to those of us in
the position of teaching.

There is a concept that I have named "the learning ladder", which is based on my own experiences
both in the public school system and later as I ventured to understand functional programming and
category theory. It goes something like this:

Imagine that learning was akin to climbing up a ladder. Each rung on the ladder represents some
concept, let's be simple and say each step is a math concept. At the very bottom of the ladder, we
have really basic concepts like the counting numbers 1-10. At the top of the ladder (I'm guessing
here), we have stuff like topology, group theory, category theory, etc.

In the beginning, we all start off at the bottom; it's pretty obvious that we can't learn how to add
or subtract if we don't yet know how to even count. This aspect of teaching is quite well understood
by educators and learners for the most part. It's only natural that certain levels of instruction
assume prerequisite understandings have been met. We wouldn't try to teach calculus to a student who
is still learning how to multiply. That would be like setting up a ladder for someone to climb, but
the first 30 rungs are missing; there's no way they're gonna jump high enough to reach that first
rung so far above them.

So this is fine, we all understand this well. But the place where we get into trouble has to do with
a different type of issue known as "The curse of knowledge". Simply put, the curse of knowledge is
the inability to explain a concept to a learner in a way that is easy for them to understand; this
is because the teacher understands the concept so well that they can no longer understand what the
learner struggles with or why. But I believe the curse of knowledge really stems from one simple
problem: putting the rungs on the ladder too far apart.

There's a funny video I saw a while back (which I would link here but I can't find it) with a title
to the effect "How to draw a horse in 3 easy steps". In the video, the "teacher" walks you through
the process: "Step 1: Draw a circle Step 2: Draw two more smaller circles, Step 3: Draw a horse"
This video, though a comically extreme example, gets at the very heart of the curse of knowledge.
Too many steps were skipped between 2 and 3; step 3 really should've been more like step 10.

The place I've experienced this the most (and most recently) has been amongst the functional
programming communities, although I believe this is common within k-16 education system as well.
It's of course not as extreme as the above example, but many articles written with the intent to
teach have tended to gloss over steps during their writing. They lead the reader up the ladder:

- Step 1
- Step 2
- Step 4
- Step 5

This is equivalent to having a ladder with missing steps; these missing steps can create difficulty.
If the learner is mature enough to persevere, they may be able to make the leap upward despite the
deficit, but even the most mature learners may be defeated if the jump is too great.

So with all of that said, the solution to the curse seems simple: we need to be more elaborate in
our explanations; ensuring that we have sufficiently lined our ladder with enough steps that
learners do not have to struggle to make their ascent.

There was an article I read one time that did this very well. At the beginning, they started with
some initial premise; as I read it, I thought to myself "Well that's obvious". As I continued to
follow through each step of the way, everything continued to be obvious. Before I knew it, I had
reached the end of the article. The whole thing absorbed piece by piece seemed absurdly simple, but
suddenly I had a firm understanding of this concept which had previously been very difficult. This
is the type of teaching we should strive for! Each step on the ladder so close to the previous, that
we don't even realize how far we've come until we look back.

It can be really easy to skip steps that seem self-explanatory, but if we take the time to really
hold the learners hand and guide them through the entire process, we will find more success in our
teaching. This is the reason I started writing my book. I want to create the perfect learning
ladder; create a journey that's so easy to follow from start to finish, that anyone could learn to
program and think to themselves at the end "Oh so that's all this was the whole time huh?".

So that's it for this one! I hope my writing has been enlightening for someone. In part 2 of this
series, I'll talk about "The M pyramid of learning".
