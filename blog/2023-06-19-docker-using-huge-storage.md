---
tags: [blog]
date: 2023-06-19
---

# Docker is using up all of my storage!

So the other day, I was installing some new vsts to my computer, when I noticed on "This PC" that
my main system drive only had 32gb of storage space remaining. My system hd isn't huge, but I have
around 500gb of space and I was pretty sure I wasn't using all of this. Upon digging around, I
eventually was lead to docker's ext4.vhd using up a whopping 160gb! How did this happen?

**Note**: This is on windows.

## Docker doesn't give you your space back

This solution comes from: https://stackoverflow.com/questions/64068185/docker-image-taking-up-space-after-deletion

Docker uses a wsl and virtual hds to storage it's images and containers (or whatever). Apparently,
even when you prune the system, the vhd doesn't recompact, it just continues to use up all the
space. So in order to fix this, you need to manually compact the vhd yourself.

**Step 1.**
First, Prune your system to clear out unused stuff

```
docker system prune -a
docker volume rm $(docker volume ls -q -f dangling=true)
```

**Step 2.**
Shut down docker server (if you're using docker-desktop, just quit out).  
Also shutdown wsl

```
wsl --shutdown
```

**Step 3.**
Compact the vhd (for me, located in C:\Users\{YourUser}\AppData\Local\Docker\wsl\data)

```
diskpart
select vdisk file="C:\Users\{YourUser}\AppData\Local\Docker\wsl\data\ext4.vhdx"
attach vdisk readonly
compact vdisk
detach vdisk
exit
```

And thats it! My docker vhd file went from taking up 160gb of space to only 11gb! Hope this helps!
