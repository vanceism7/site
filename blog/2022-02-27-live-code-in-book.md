---
tags: [blog]
date: 2022-02-27
---

# Live code blocks in the Programming Book

Its been a while since I dedicated a good block of time to working on my book. A big part of that is
trying to really hone in on how to teach functions. (A thought I have about this is perhaps reading
through SICP to see how they teach them, although I have my own notions anyways.)

But over the weekend, I managed to hack my way through the internals of mdbook, and drafted a pull
request adding support for Purescript in the code editor. What does that mean? It means the code
blocks are live! You can write Purescript code right in the book, and press the play button to run
the code and see what it does.

![](/img/live-book-code.gif)

It's not exactly perfect, but it's a drastic improvement over the old copy-paste style we had going
before. A big thanks to all the brilliant engineers in the Purescript community for creating the
tooling necessary to even make this possible!

This has me really excited and ready to clean up the book of all the now obsolete directions, and to
tackle this functions chapter and move forward!

So yea! Just wanted to write that update. This programming book is near and dear to my heart, and I
hope to bring a lot of new would-be software engineers into the fold through this book.

Cheers!
