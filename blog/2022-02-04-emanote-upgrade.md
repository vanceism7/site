---
tags: [blog]
date: 2022-02-04
---

# Emanote Site Upgrade

Yep, I've upgraded the site again. Lately, I've been thinking about writing some blog posts which
were less personal and more academic in nature; I didn't want to get these different types of posts
mixed up though so I was holding out until I had sometime to figure out how I wanted to separate
them.

But then I remembered... a while back on the [Zulip Haskell][zulip] chat, there was a guy named
[srid] who had been working on some interesting note-taking methods and building tools to facilitate
those endeavors. I decided to lookup his github and found that he's built this new static site
generator called [Emanote] - it's very cool! In my experience so far, it's worked with very little
issue and getting up and running was _fast_.

So here we are. The site's been upgraded. Looking forward to migrating my guides here!

Cheers!

[zulip]: https://funprog.zulipchat.com
[srid]: https://www.srid.ca
[Emanote]: https://emanote.srid.ca/
