---
tags: [blog]
date: 2022-01-19
---

# Convenience is King and Polish is quality

Ive noticed this strange phenomenon recently... There's a phrase that goes "Use the best tool for
the job", but what does that really mean? Initially I would have thought that meant the tool with
the greatest capacity to accomplish the target task, but it seems I've thought wrong. When it comes
to tool selection, I've seen it time and time again that there appear to be two attributes which
exhibit greater influence than capacity: convenience and polish.

Before we continue, let me define what I believe these terms mean:

- I'd define **convenience** to mean how quick and easy it is to setup and start using a tool, and
  how much cognitive lift it takes to utilize that tool on a daily basis.
- **Polish** is how nice the tools UI is, and how well it accomplishes it's intended tasks without
  encountering errors/bugs/malfunctions.

Convenience and polish seem to be such weighty factors in our daily lives that we're willing to give
up just about anything for it; things like financial well-being, privacy, and even our health!
Companies like Google, Apple, and Amazon (either consciously or instinctively) know this and use it
to control our choices every day.

Anyways, I digress! I've seen it a few times that convenience and polish have won out against (what
I consider to be) the better tool. Here are a few examples.

### [Haskell] vs [Blub]

I discovered the joy of functional programming and Haskell in specific a few years back. The
language is very fun to work in, and has probably one of the greatest capacities to write software
that I've ever seen; so why has it not caught on? Up until recently, the development experience has
been wrought with difficulty. Installing was hard, specifying dependencies was hard, compiling can
be quite slow, and finding libraries is pretty confusing. But that's not to say that any of it was
actually major blocker, rather, it's just very inconvenient. Documentation is not convenient to
find, multiple installers with different issues was a hassle, and probably the biggest gotcha was
the IDE experience. For the longest time, developers essentially ran an auto-compile loop in
terminal as a bit of an IDE hack - definitely not convenient and about as unpolished as you can get.
The Haskell community considered these to be minor issues (and maybe they are), but to the average
guy, that inconvenience just makes the whole thing not worth it. Why deal with that when you could
just spin up C\#/JavaScript/Python/Go and you're off to the races?

In defense of the Haskell community, there have been a lot of initiatives to improve these issues
lately, and the Haskell IDE experience has improved by leaps and bounds. Haskell language server
installs automatically and the polish is great. With those improvements alone, I consider Haskell's
dev story to be better than Purescript now. Anyways, moving on!

### VsCode vs Emacs

Back when I was working at CitizenNet, there was one particular day where I was pair programming
with a colleague. At the time, the defacto code editor of choice on our team was emacs (because it
has a decent IDE experience for Purescript). However, I had seen how nice LSP worked on VsCode and
decided to try to make the switch. Purescript's LSP implementation was in good enough shape that it
worked without too much hassle. As my colleague and I worked through a ticket, the compiler spit an
error at us as it usually does, showing under which file the error occured along with the line
number. Normally in emacs, we would use the file open command, locate the file, and jump to the
error. But in VsCode, I could ctrl+click the error location in the terminal and it took me straight
there! Is that a trivial feature? Definitely! But my colleague was thoroughly impressed by it. I
don't know if this was the tipping point for him or not, but he soon made the switch himself, the
rest of the team following not long after.

### [Zulip] vs Google Chat

Zulip is an amazing chat tool with an interesting model for organizing conversations. Compared to
Google chat, I'd say Zulip definitely has a _much_ greater capacity as a team communication app.
However, the convenience and polish on Google chat is just a little better. Convenience-wise, Google
chat wins because there's no software installation required, nor do you need to create an account
for it. (This is sort of unfair, you _do_ need a Gmail account, but Google has already secured
themselves as the most popular email provider, so most people have an account by default.)
Polish-wise, Zulip has had intermittent issues where notifications wouldn't come through. These
issues were enough to convince my team to jump ship from Zulip to Google Chat. I'm slightly pained
to see it as I'm a big fan of Zulip, but I don't blame them at all!

### [Erlang] vs [Elixir]

I've been taking some time as of late to learn the BEAM languages since I hear they're pretty
impressive for writing server applications. I was unsure if I should go with Elixir or Erlang, but a
quick trial of each promptly led me to Elixir. What were the deciding factors? Erlang's VsCode
plugins just didn't seem to work very well. ErlangLS couldn't infer type specs (it kept saying to
check the logs to find out why). Also `rebar` was ok, but seemed a little confusing to work with.
Elixir wasn't perfect in comparison, but ElixirLS plugin worked right out of the box, and `mix` was
a good amount easier to work with too.

## Conclusion

So why did I write all of this? I've long seen some discussions in the haskell community about what
can be done to foster greater adoption. At the same time, I've also dealt first hand with trying to
get my own team at work to adopt technologies that promised overall improvement of our processes. I
(of course) would always recommand the tools I considered to be the best, but even though these
tools were very good, getting the team to use them has not often been easy. Eventually, we landed on
using Google Chat as our internal communication medium, and Zoho Bigin as our Project management
tool (despite Bigin not _actually being_ a Project management tool). I spent a good amount of time
trying to get the team to adopt certain technologies, but it only took about a day to adopt Bigin
and Google Chat. The difference was the convenience and polish. These tools may not be the most
powerful ones to aid us in our work, but they were the most convenient, and they worked without
issue.

If we want to foster greater adoption of any type of technology, these two attributes should
not be overlooked. Indeed, I believe they may actually be the most important ones.

[Haskell]: https://www.haskell.org/
[Blub]: https://www.quora.com/What-is-the-Blub-programming-language
[Zulip]: https://zulip.com/
[Erlang]: https://erlang.org/
[Elixir]: https://elixir-lang.org/
